const TodoItem = (props) => {
    const {value}=props
    return(
        <div className="TodoItem">
            {value}
        </div>
    )

}

export default TodoItem;