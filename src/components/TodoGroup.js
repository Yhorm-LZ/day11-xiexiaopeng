import TodoItem from "./TodoItem";

const TodoGroup = (props) => {
    const {toDoList}=props
    return(
        <div className="TodoGroup">
            {
                toDoList.map((value,index)=><TodoItem key={index} value={value}/>)
            }
        </div>
    )

}

export default TodoGroup;