import {useState} from "react";

const TodoGenerator = (props) => {
    const {handlerTodoList}=props
    const [input,setInput]=useState("")
    const addTodoList=()=>{
        handlerTodoList(input)
    }

    function updateInput(event) {
        setInput(event.target.value)
    }

    return(
        <div>
            <input type={"text"} value={input} onChange={updateInput}/>
            <button onClick={addTodoList}>add</button>
        </div>
    )

}

export default TodoGenerator;