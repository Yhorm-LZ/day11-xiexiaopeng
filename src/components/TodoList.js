import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";
import {useState} from "react";

const TodoList = () => {
    const [toDoList,setToDoList]=useState([])
    const handlerTodoList=(input)=>{
        toDoList.push(input)
        setToDoList([...toDoList])
    }
    return(
        <div>
            <h3>Todo List</h3>
            <TodoGroup toDoList={toDoList}/>
            <TodoGenerator toDoList={toDoList} handlerTodoList={handlerTodoList}/>
        </div>
    )

}

export default TodoList;